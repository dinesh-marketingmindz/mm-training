

<!-- ============================================== -->


<!-- Super Container | Footer Widget Space (Optional) -->
<?php if (get_option_tree('footer_widgets') != 'No') : ?>

	<?php  // Set the columns class for each module.
		if(ot_get_option('footer_columns_count') != '') : 
		$header_columns = ot_get_option('footer_columns_count');
		else : 
		$header_columns = "";
		endif;
	?> 

<div class="super-container full-width" id="section-footer">

<!-- 960 Container -->
<div class="container">

<!-- footer -->
<footer>

	<div class="sixteen columns alpha omega" id="footer-row"> 
		<div class="<?php echo $header_columns; ?>">
		<?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('footer-widget-1') ) ?>
		</div>
		<div class="<?php echo $header_columns; ?>">
		<?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('footer-widget-2') ) ?>
		</div>
		<div class="<?php echo $header_columns; ?>">
		<?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('footer-widget-3') ) ?>
		</div>
		<div class="<?php echo $header_columns; ?>">
		<?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('footer-widget-4') ) ?>
		</div>
	</div>
</footer>
<!-- /End Footer -->

</div>
<!-- /End 960 Container -->
</div>
<!-- /End Super Container -->
<?php endif; ?>


<!-- ============================================== -->


<!-- Super Container - SubFooter Space -->

<!-- /End Super Container -->


<!-- ============================================== -->


<?php wp_footer(); ?>
</body>
</html>