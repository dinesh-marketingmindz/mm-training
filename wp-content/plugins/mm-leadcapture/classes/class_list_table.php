<?php
if ( ! class_exists( 'WP_List_Table' ) )
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
	require_once( ABSPATH . 'wp-admin/includes/screen.php' );

class LC_Contact_Form_List_Table extends WP_List_Table {
   /**
    * Constructor, we override the parent to pass our own arguments
    * We usually focus on three parameters: singular and plural labels, as well as whether the class supports AJAX.
    */
    function __construct() {
       parent::__construct( array(
      'singular'=> 'post', //Singular label
      'plural' => 'posts', //plural label, also this well be one of the table css class
      'ajax'   => false //We won't support Ajax for this table
      ) );
    }
	  /**
	   * Add extra markup in the toolbars before or after the list
	   * @param string $which, helps you decide if you add the markup after (bottom) or before (top) the list
	   */
	function extra_tablenav( $which ) {
		if ( $which == "top" ){
			//The code that goes before the table is here
			echo '<h2>';
			echo esc_html( __( 'Contact Forms' ) );
			$url = admin_url( 'admin.php?page=add_contact_form' );
			echo ' <a href="' . $url .'" class="add_new_h2" >Add New</a>';
			echo '</h2>';
		}
		if ( $which == "bottom" ){
			//The code that goes after the table is there
			echo "5 records per page";
		}
	}
	
	function get_columns() {
		$columns = array(
		'id' => __('ID'),
		'title' => __( 'Title' ),
		'author' => __( 'Author' ),
		'date' => __( 'Date' ),
		'shortcode' => __( 'Shortcode' ) );
		return $columns;
	}
	/**
	* Decide which columns to activate the sorting functionality on
	* @return array $sortable, the array of columns that can be sorted by the user
	*/
	public function get_sortable_columns() {
		$sortable = array(
		//'id' => 'ID',
		'title' => array( 'title', true ),
		//'author' => array( 'author', false ),
		'date' => array( 'date', false ) 
		//'shortcode' => 'shortcode'
		 );
		return $sortable;
	}
	/**
	 * Prepare the table with different parameters, pagination, columns and table elements
	 */
	function prepare_items() {
		global $wpdb, $_wp_column_headers;
		$screen = get_current_screen();
		/* -- Preparing your query -- */
		$query = "SELECT * FROM $wpdb->posts WHERE post_type='".LC_CUSTOM_POST_TYPE."' AND post_status='publish'";
		/* -- Ordering parameters -- */
		//Parameters that are going to be used to order the result
		
		//$orderby = !empty($_GET["orderby"]) ? mysql_real_escape_string($_GET["orderby"]) : 'ASC';
		//$order = !empty($_GET["order"]) ? mysql_real_escape_string($_GET["order"]) : '';
		//if(!empty($orderby) & !empty($order)){ $query.=' ORDER BY '.$orderby.' '.$order ; }


		if ( ! empty( $_REQUEST['orderby'] ) ) {
			if ( 'title' == $_REQUEST['orderby'] )
				$orderby = 'post_title';
			elseif ( 'author' == $_REQUEST['orderby'] )
				$orderby = 'post_author';
			elseif ( 'date' == $_REQUEST['orderby'] )
				$orderby = 'post_date';
		}

		if ( ! empty( $_REQUEST['order'] ) ) {
			if ( 'asc' == strtolower( $_REQUEST['order'] ) )
				$order = 'ASC';
			elseif ( 'desc' == strtolower( $_REQUEST['order'] ) )
				$order = 'DESC';
		}

		//$query.=' ORDER BY '.$orderby.' '.$order ;
		if(!empty($orderby) & !empty($order)){ $query.=' ORDER BY '.$orderby.' '.$order ; }
		//print_r($query);

		/* -- Pagination parameters -- */
		//Number of elements in your table?
		$totalitems = $wpdb->query($query); //return the total number of affected rows
		//How many to display per page?
		$perpage = 5;
		//Which page is this?
		//$paged = !empty($_GET["paged"]) ? mysql_real_escape_string($_GET["paged"]) : '';

		//Page Number
		if(empty($paged) || !is_numeric($paged) || $paged<=0 ){ $paged=1; }
		if ( ! empty( $_REQUEST['paged'] ) ) {
			$paged = $_REQUEST['paged'];
		}
		//How many pages do we have in total?
		$totalpages = ceil($totalitems/$perpage);
		//adjust the query to take pagination into account
		if(!empty($paged) && !empty($perpage)){
			$offset=($paged-1)*$perpage;
			$query.=' LIMIT '.(int)$offset.','.(int)$perpage;
		}
		/* -- Register the pagination -- */
		$this->set_pagination_args( array(
			"total_items" => $totalitems,
			"total_pages" => $totalpages,
			"per_page" => $perpage,
		) );
		//The pagination links are automatically built according to those parameters
		/* -- Register the Columns -- */
		$columns = $this->get_columns();
		$hidden = array();
		$sortable = $this->get_sortable_columns();
		$this->_column_headers = array($columns, $hidden, $sortable);
		/* -- Fetch the items -- */
		$this->items = $wpdb->get_results($query);
		
	}
	
	function display_rows() {
		//Get the records registered in the prepare_items method
		$records = $this->items;
		//Get the columns registered in the get_columns and get_sortable_columns methods
		list( $columns, $hidden ) = $this->get_column_info();
		if(empty($_REQUEST['paged']) || $_REQUEST['paged'] ==1 )
		{ $var_id = 1; }
		else{ $var_id = ($_REQUEST['paged'] * 5) - 4; }
		//Loop for each record
		if(!empty($records)){
			foreach($records as $rec){
				//Open the line
				echo '<tr id="record_'.$rec->ID.'">';
				foreach ( $columns as $column_name => $column_display_name ) {
					//Style attributes for each col
					$class = "class='$column_name column-$column_name'";
					$style = "";
					if ( in_array( $column_name, $hidden ) ) $style = ' style="display:none;"';
					$attributes = $class . $style;
					//user_name
					$userinfo=get_userdata($rec->post_author);
					$user=$userinfo->display_name;
					//edit link
					$editlink  = "<a href='".site_url()."/wp-admin/admin.php?page=add_contact_form&action=edit&post=".$rec->ID."'>Edit</a>";
					$deletelink  = "<a href='".site_url()."/wp-admin/admin.php?page=contact_form&action=delete&post=".$rec->ID."' onclick='return confirm(\"Are you sure?\");'>Delete</a>";
					//shortcode generator...
					$shortcode="<input type='text' onclick='this.select();' readonly='readonly' style='width:100%' value='[lc_contact-form id=\"$rec->ID\" title=\"$rec->post_title\" ]' />";
					//Display the cell
					switch ( $column_name ) {
						case "id":  echo '<td '.$attributes.'>'.$var_id++.'</td>';   break;
						case "title": echo '<td '.$attributes.'>'.stripslashes($rec->post_title).'<br>'.$editlink.'&nbsp;&nbsp;'.$deletelink.'</td>'; break;
						//case "shortcode": echo '< td '.$attributes.'>'.stripslashes($rec->post_shortcode).'< /td>'; break;
						case "author": echo '<td '.$attributes.'>'.$user.'</td>'; break;
						case "date": echo '<td '.$attributes.'>'.$rec->post_date.'</td>'; break;
						case "shortcode": echo '<td '.$attributes.'>'.$shortcode.'</td>'; break;
					}
				}
			//Close the line
			echo'</tr>';
			}
		}
	}
}
?>