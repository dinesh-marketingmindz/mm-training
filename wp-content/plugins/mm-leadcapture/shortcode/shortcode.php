<!-- Sliding div starts here -->
    <div id="lc-form-popup" style="right:-264px;">
        <div id="sidebar" onclick="open_panel()">
            <img src="<?php echo LEAD_CAPTURE_IMAGE . 'contact.png'; ?>"/>
        </div>
        <div id="header1" class="mm-lead-form">
                <form action="#" method="post" id="leadForm">
                    <span class="close-popup" onclick="close_panel()">&#10006;</span>
                    <h2>Contact Form</h2>
                    <span style="err" id="errMsg"></span>
                    <span style="succ" id="succMsg"></span>
                    <input type="text" id="fname" name="fname" placeholder="First Name"/>
                    <input type="text" id="lname" name="lname" placeholder="Last Name"/>
                    <input type="text" id="email" name="email" placeholder="Email"/>
                    <input type="text" id="phone" name="phone" placeholder="Phone"/>                    
                    <textarea id="msg" name="msg" placeholder="Message"></textarea><br/>
                    <input type="hidden" name="lc_js_url" id="lc_js_url" value="<?php echo LEAD_CAPTURE_URL; ?>">
                    <input type="hidden" name="lc_site_url" id="lc_site_url" value="<?php echo get_option('siteurl'); ?>">
                    <input type="hidden" name="lc_redirect_url" id="lc_redirect_url" value="<?php echo get_option('lc_thankyou_page'); ?>">
                    <button type="button" id="submit" onclick="submitEnquery();">Send Message</button>
                </form>
        </div>
    </div>
    
    <script>
    jQuery(document).ready(function(){
            //setTimeout(open_panel,10000);
      if(localStorage.getItem('popState') != 'shown'){
        //$("#popup").delay(2000).fadeIn();
          setTimeout(open_panel,10000);
        localStorage.setItem('popState','shown')
    }
      });
        </script>
    <!-- Sliding div ends here -->