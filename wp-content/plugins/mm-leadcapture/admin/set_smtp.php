<?php 
if(isset($_POST['submit'])){
	$return_path = isset($_POST['mmcf_mail_set_return_path'])?$_POST['mmcf_mail_set_return_path']:'';
	update_option( 'mmcf_mail_from', $_POST['mmcf_mail_from'] );
	update_option( 'mmcf_mail_from_name', $_POST['mmcf_mail_from_name'] );
	update_option( 'mmcf_mailer', $_POST['mmcf_mailer'] );
	update_option( 'mmcf_smtp_host', $_POST['mmcf_smtp_host'] );
	update_option( 'mmcf_smtp_port', $_POST['mmcf_smtp_port'] );
	update_option( 'mmcf_smtp_ssl', $_POST['mmcf_smtp_ssl'] );
	update_option( 'mmcf_smtp_auth', $_POST['mmcf_smtp_auth'] );
	update_option( 'mmcf_smtp_user', $_POST['mmcf_smtp_user'] );
	update_option( 'mmcf_smtp_pass', $_POST['mmcf_smtp_pass'] );
	update_option( 'mmcf_mail_set_return_path', $return_path );
}
?>
<div class="wrap">
    <h2>Advanced Email Options</h2>
    <form method="post" action="">
        <input type="hidden" id="" name="" value=""><input type="hidden" name="" value="">
        <table class="optiontable form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row"><label for="mmcf_mail_from">From Email</label></th>
                    <td><input name="mmcf_mail_from" type="text" id="mmcf_mail_from" value="<?php echo get_option('mmcf_mail_from',''); ?>" size="40" class="regular-text">
                		<!--<span class="description">You can specify the email address that emails should be sent from. If you leave this blank, the default email will be used.</span></td> -->
                </tr>
                <tr valign="top">
                    <th scope="row"><label for="mmcf_mail_from_name">From Name</label></th>
                    <td><input name="mmcf_mail_from_name" type="text" id="mmcf_mail_from_name" value="<?php echo get_option('mmcf_mail_from_name',''); ?>" size="40" class="regular-text">
               			<!--<span class="description">You can specify the name that emails should be sent from. If you leave this blank, the emails will be sent from WordPress.</span></td> -->
                </tr>
            </tbody>
        </table>

        <table class="optiontable form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row"><label for="mmcf_smtp_host">SMTP Host</label></th>
                    <td><input name="mmcf_smtp_host" type="text" id="mmcf_smtp_host" value="<?php echo get_option('mmcf_smtp_host',''); ?>" size="40" class="regular-text"></td>
                </tr>
                <tr valign="top">
                    <th scope="row"><label for="mmcf_smtp_port">SMTP Port</label></th>
                        <td><input name="mmcf_smtp_port" type="text" id="mmcf_smtp_port" value="<?php echo get_option('mmcf_smtp_port',''); ?>" size="6" class="regular-text"></td>
                </tr>
                <tr valign="top">
                    <th scope="row">Encryption </th>
                        <td><fieldset><legend class="screen-reader-text"><span>Encryption</span></legend>
                            <span class="description">All E-mails will be sent using SSL encryption</span></fieldset></td>
                </tr>
                <tr valign="top">
                    <th scope="row"><label for="mmcf_smtp_user">Username</label></th>
                        <td><input name="mmcf_smtp_user" type="text" id="mmcf_smtp_user" value="<?php echo get_option('mmcf_smtp_user',''); ?>" size="40" class="code"></td>
                </tr>
                <tr valign="top">
                    <th scope="row"><label for="mmcf_smtp_pass">Password</label></th>
                        <td><input name="mmcf_smtp_pass" type="password" id="mmcf_smtp_pass" value="<?php echo get_option('mmcf_smtp_pass',''); ?>" size="40" class="code"></td>
                </tr>
            </tbody>
        </table>
    	<p class="submit"><input type="submit" name="submit" id="submit" class="button-primary" value="Save Changes"></p>
    	<input type="hidden" name="action" value="update">
    	<input type="hidden" name="option_page" value="email">
    </form>
</div>