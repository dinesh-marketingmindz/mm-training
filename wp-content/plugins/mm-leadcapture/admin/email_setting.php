<?php
/**
 * Setting section of leadcapture Plugin.
 * 
 *
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
if (isset($_POST['emailsettingSubmit'])) {

  //print_r($_REQUEST);
    
   $lc_from_name = $_REQUEST['lc_from_name'];
   update_option('lc_from_name',$lc_from_name );

   $lc_from_email_id = $_REQUEST['lc_from_email_id'];
   update_option('lc_from_email_id',$lc_from_email_id );

   $lc_To_email = $_REQUEST['lc_To_email'];
   update_option('lc_To_email',$lc_To_email );

   $lc_email_subject = $_REQUEST['lc_email_subject'];
   update_option('lc_email_subject',$lc_email_subject );

   $lc_checkme = $_REQUEST['lc_checkme'];
   update_option('lc_checkme',$lc_checkme );

   $lc_thankyou_page = $_REQUEST['lc_thankyou_page'];
   update_option('lc_thankyou_page',$lc_thankyou_page );

   if(($_REQUEST['lc_from_name']!= null || $_REQUEST['lc_from_name']!= '') && ($_REQUEST['lc_from_email_id']!= null || $_REQUEST['lc_from_email_id']!= '') && ($_REQUEST['lc_To_email']!= null || $_REQUEST['lc_To_email']!= '')){

     //$lc_massege = "Saved email setting";
     $lc_massege = '<div class="updated settings-error notice is-dismissible" id="setting-error-settings_updated"> 
<p><strong>Settings saved.</strong></p><p> Add shortcode in footer.php file of theme "[crmLeadcapture]".</p><button class="notice-dismiss" type="button"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';

   }else{
    //$lc_massege = " fill the all fields";
    $lc_massege = '<div class="error settings-error notice is-dismissible" id="setting-error-invalid_admin_email"> 
<p><strong>Please fill the all fields.</strong></p><button class="notice-dismiss" type="button"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
       }
}
?>
 <div id="wpwrap wrap">
    <div id="wpbody-content" tabindex="0" aria-label="Main content">
        <div class="wrap">
            <h2><?php echo __('Email Settings', 'leadcapture'); ?></h2>

            <?php echo $lc_massege; ?>


            <form novalidate="novalidate" action="" method="post" id="email_form">
                <table class="form-table" width="800">
                    
                   <tr>
                        <th><?php echo __('From Name', 'leadcapture'); ?>
                         </th>
                        <td><input type="text" placeholder="from name" class="regular-text" name="lc_from_name" value="<?php echo get_option('lc_from_name') ?>">
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('From Email id', 'leadcapture'); ?>
                        </th>
                        <td class="input-icon"><input type="email" placeholder="from email id" name="lc_from_email_id" class="regular-text" value="<?php echo get_option('lc_from_email_id') ?>"></td>
                    </tr>
                    <tr>
                        <th><?php echo __('To Email', 'leadcapture'); ?>
                         </th>
                        <td><input type="email" placeholder="To email" class="regular-text" name="lc_To_email" value="<?php echo get_option('lc_To_email') ?>">
                        </td>
                    </tr> 
                    <tr>
                        <th><?php echo __('Subject', 'leadcapture'); ?>
                         </th>
                        <td><input type="text" placeholder="subject" class="regular-text" name="lc_email_subject" value="<?php echo get_option('lc_email_subject') ?>">
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Redirect Page Url(optional)', 'leadcapture'); ?>
                         </th>
                        <td><input type="text" placeholder="Thank-you Page" class="regular-text" name="lc_thankyou_page" value="<?php echo get_option('lc_thankyou_page') ?>">
                        </td>
                    </tr>
                    <tr>
                    <th></th>
                      <td>
                        <input type="checkbox" name="lc_checkme" value="1" <?php checked( $get_option['lc_checkme'], 1, false );?>  <?php echo get_option('lc_checkme') == 1 ? 'checked' : ''; ?> > Check for send an email 
                       </td>                          
                    </tr>
                     
                    <tr>
                        <td>
                        <?php wp_nonce_field(); ?>
                        <input type="submit" value="Save Setting" class="button button-primary" id="submitbtnform" name="emailsettingSubmit" >
                        <input type="hidden" name="action" value="email_setting_submit" >
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>