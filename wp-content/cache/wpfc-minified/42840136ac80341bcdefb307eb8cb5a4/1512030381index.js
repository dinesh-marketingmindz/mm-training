// source --> https://training.technologymindz.com/wp-content/plugins/mm-leadcapture/js/scripts.js?ver=4.7.8 
function AddMore(){
  var html = '<input type="text" placeholder="Enter Form Fields" class="regular-text" name="lc_form_field_name[]" >';
  jQuery("#append").append(html);
           
}

//validation function for add new form and edit form...
function add_form_validation()
{
	var title=document.getElementById('cf8-title').value;
	if(title=='')
	{
		alert('Please Fill Title for the form');
		return false;
	}
	
	var form=document.getElementById('cf8-form').value;
	if(form=='')
	{
		alert('Please Fill Something into the form');
		return false;
	}
	
	var mail_to=document.getElementById('mail_recipient').value;
	if(mail_to=='')
	{
		alert('Please Fill Mail Recipient(To:)');
		return false;
	}
	
	var mail_subject=document.getElementById('mail_subject').value;
	if(mail_subject=='')
	{
		alert('Please Fill Mail Subject(Subject:)');
		return false;
	}
	
	var mail_msg=document.getElementById('mail_body').value;
	if(mail_msg=='')
	{
		alert('Please Fill Mail Message Body(Body:)');
		return false;
	}
	
	return true;
}


//jquery starts from here...

jQuery(document).ready(function(){
jQuery("#tag_generator").change(function(){
    $select_val=jQuery(this).val();
	if($select_val==0)
	{
		jQuery("#add_extra_fields").hide();   
	}
	else
	{
		jQuery("#extra_label").text($select_val.toUpperCase());
		//$(this).text($(this).attr("typename"));
		jQuery("#add_extra_fields").show();
		jQuery("#extra_shortcodefield_output").val("");
		jQuery("#extra_shortcodemail_output").val("");
		jQuery("#extra_id").val("");
		jQuery("#extra_name").val("");
		jQuery("#extra_class").val("");
		jQuery("#extra_val").val("");
		jQuery("#extra_ta_val").val("");
		jQuery('#placeholder').attr('checked', false);
		jQuery('#required').attr('checked', false);
		
		if($select_val=="select" || $select_val=="checkbox" || $select_val=="radio" || $select_val=="textarea" || $select_val=="file" || $select_val=="captcha")
		{
			jQuery("#extra_ta_val").show();
			jQuery("#extra_div_val").hide();
			jQuery("#req").show();
			jQuery("#label_val").show();
			jQuery("#placeholder_div").show();
			//if($select_val=="checkbox")
			//{
			//	$("#req").hide();
			//}
			if($select_val=="captcha" || $select_val=="file" )
			{
				jQuery("#extra_ta_val").hide();
				jQuery("#label_val").hide();
			}
		}
		else   
		{
			jQuery("#placeholder_div").show();
			jQuery("#extra_ta_val").hide();
			jQuery("#extra_div_val").show();
			jQuery("#req").show();
			jQuery("#label_val").show();
			
			if($select_val=="submit")
			{
				jQuery("#placeholder_div").hide();
			}
			
		}
	}
});

//setting the shortcode value....
jQuery("#extra_shortcodefield_output").focus(function(){

$main=jQuery("#tag_generator").val();

if(jQuery("#required").is(':checked'))
   { $req="*";  }// checked
else
   { $req=""; }

$name=jQuery("#extra_name").val();
if($name==''){
 document.getElementById("extra_shortcodefield_output").blur();
 alert("first choose name of the field...");return false;}
 var $name = $name.toLowerCase();
 var $name = $name.replace(/ /g,"-");


$extra_val=jQuery("#extra_val").val();
 if(jQuery('#placeholder').attr('checked')){
 if($extra_val=='') {
 	 document.getElementById("extra_shortcodefield_output").blur();
 	alert("first choose default name placeholder ");return false;}
 }
 else
 { 	$placeholder=""; }

if(jQuery("#extra_id").val()==''){$id="";}
else {$id=" id:" +jQuery("#extra_id").val();}

if(jQuery("#extra_class").val()==''){$class="";}
else {$class=" class:" +jQuery("#extra_class").val();}

//setting values according the tag type in html...
if($select_val=="select" || $select_val=="checkbox" || $select_val=="radio")
{
	if(jQuery("#extra_ta_val").val()==''){$val="";}
	else {
	$val="";
	$value = jQuery('#extra_ta_val').val().split("\n");
		for($v=0; $v<$value.length; $v++)
		{
		$val +=' "' +$value[$v] +'"';
		}
	}
}
else
{
	if(jQuery("#extra_val").val()==''){$val=""; $placeholder="";}
	else {
		if(jQuery('#placeholder').attr('checked'))
   		{ $placeholder=' placeholder"' +jQuery("#extra_val").val() +'"';  }// checked
		else
   		{ $placeholder=""; }
   		$val=' value"' +jQuery("#extra_val").val() +'"';
	
	}
}

{
jQuery("#extra_shortcodefield_output").val("[" +$main+$req +" " +$name +$id +$class +$val + $placeholder+"]");
}
jQuery(this).select();
});

//setting the shortcode mail value....
jQuery("#extra_shortcodemail_output").focus(function(){
	$name=jQuery("#extra_name").val();
if($name==''){
	document.getElementById("extra_shortcodemail_output").blur();
	alert("first choose name of the field...");return false;}
var $name = $name.toLowerCase();
var $name = $name.replace(/ /g,"-");
{
jQuery("#extra_shortcodemail_output").val("[" +$name +"]");
}
jQuery(this).select();
});

});