<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache


/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'WPCACHEHOME', '/home/technologymindz/public_html/training/wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager
define('DB_NAME', 'devmindz_technolo_phptraining');

/** MySQL database username */
//define('DB_USER', 'technolo_MT');
define('DB_USER', 'devmindz_technol');

/** MySQL database password */
//define('DB_PASSWORD', 'MT123!@#');
define('DB_PASSWORD', 'mmtraining123!@#');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '?J5)tl4G^.UiIpCRp jd*J$x^Pi3H=j}o<1M)}qrl<?(Ha:[.% A&%)#bc*XCtT&');
define('SECURE_AUTH_KEY',  ' ye)%O/- A6kGzc<&-v>=6B/uz]*EjX*BdN&*e4q(V9H#}wYakzr. crvNT3y;Zt');
define('LOGGED_IN_KEY',    'GjLAZGo{*-6(YuE]%$StWHhv^3t^;Old`~S_X&]>i-}K6ju=1Rv}(N4W-J e&@u~');
define('NONCE_KEY',        'XD[o#J(U|BNj]FCT.}RUcTJ8LTH*!b2G?w3to=/PVF{7,tyR^E2y!i[mwp^5d:nB');
define('AUTH_SALT',        'sb</+zYelw}Ap+%~$(TUMm1kW%vh).O<N9b`=GZd0@J5FdZ/u>k.Y`?8KuA]#Z>T');
define('SECURE_AUTH_SALT', 'j8Vhgdo:BoyRJ<d$A 749S0/,q32_@vjV3/uMGGa^uc%us7Vu=&3;fv)82<|4ASI');
define('LOGGED_IN_SALT',   'Cd.UNlN9gL2&amr^zaiUr#TtfR&Q~JtzRJ1z}M_QQ%],<^P`#SsvR;S)pZ:-=#td');
define('NONCE_SALT',       ']9w-n%#<TOVv{P/|j [~;=]Y2G&A/M,QvcqLNU7M:zp6EYH#p8r&r&ZJSO)& `Sg');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'pr_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);
/*define('WP_HOME','http://www.training.technologymindz.com/');
define('WP_SITEURL','http://www.training.technologymindz.com/');*/

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

 //Added by WP-Cache Manager
