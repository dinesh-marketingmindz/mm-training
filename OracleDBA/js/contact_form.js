// JavaScript Document
$(document).ready(function() {
    $("#phpcontactform").submit(function(e) {
        e.preventDefault();
        var redirect = $(this).data('redirect');
        var noredirect = false;
        if ( redirect == 'none' || redirect == "" || redirect == null ) {
                noredirect = true;
        }
        var thankyou = "http://www.oracledbaexperts.com/thank-you/";
        var name = $("#name");
        var email = $("#email");
        var mobile = $("#mobile");
        var message = $("#message");
    var emailReg = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    //var phoneReg = /^(?!.*-.*-.*-)(?=(?:\d{10}$)|(?:(?=.{9,11}$)[^-]*-[^-]*$)|(?:(?=.{10,12}$)[^-]*-[^-]*-[^-]*$)  )[\d-]+$/;
        var flag = false;
        if (name.val() == "") {
            name.closest(".form-group").addClass("has-error");
            name.focus();
            flag = false;
            return false;
        } else {
            name.closest(".form-group").removeClass("has-error").addClass("has-success");
        } if (email.val() == "") {
            email.closest(".form-group").addClass("has-error");
            email.focus();
            flag = false;
            return false;
        }
    else if(!emailReg.test( email.val() )){
        email.closest(".form-group").addClass("has-error");
        email.focus();
        flag = false;
        return false;
    }
     else {
            email.closest(".form-group").removeClass("has-error").addClass("has-success");
            //flag = true;
            //$('input[type="submit"]').attr('disabled','disabled');
        }
        if (mobile.val() == "") {
            mobile.closest(".form-group").addClass("has-error");
            mobile.focus();
            flag = false;
            return false;
        } else {
            mobile.closest(".form-group").removeClass("has-error").addClass("has-success");
        } 
    /*if(!phoneReg.test( mobile.val() )){
        mobile.closest(".form-group").addClass("has-error");
        mobile.focus();
        flag = false;
        return false;
    }
    else {
        var tel=''; 
        var val =mobile.val().split(''); 
        for(var i=0;i<val.length;i++){ 
            if(i==2){val[i]=val[i]+'-'} 
            if(i==5){val[i]=val[i]+'-'} 
            tel=tel+val[i] 
        }*/
        /*$("#mobile").val();
            mobile.closest(".form-group").removeClass("has-error").addClass("has-success");
            flag = true;
            $('input[type="submit"]').attr('disabled','disabled');
        }*/
        var dataString = "name=" + name.val() + "&email=" + email.val() + "&mobile=" + mobile.val() + "&message=" + message.val();
        $(".loading").fadeIn("slow").html("<p>Loading...</p>");
        $.ajax({
            type: "POST",
            data: dataString,
            url: "contact.php",
            cache: false,
            success: function (d) {
                $(".form-group").removeClass("has-success");
             if(d == 'success') // Message Sent? Show the 'Thank You' message and hide the form
             if (noredirect) {
            $('.loading').fadeIn('slow').html('<p><font style="color:#51D026;">Form submitted successfully.</font></p>').delay(3000).fadeOut('slow');
            window.location.href = thankyou;
            $("#phpcontactform").get(0).reset();
             } else {
                 window.location.href = redirect;
             }
         else
         $('.loading').fadeIn('slow').html('<p><font style="color:#F44A4A;">Oops. Something went wrong.</font></p>').delay(3000).fadeOut('slow');

            }
        });
        return false;
    });
    $("#reset").click(function () {
        $(".form-group").removeClass("has-success").removeClass("has-error");
    });
})



